# wunderbon | smart receipts

## Documentation TypeScript-Declarations

wunderbon [**typescript-declarations**](https://basarat.gitbooks.io/typescript/docs/types/ambient/d.ts.html)  of data structures (`contracts` like `interfaces`, `models` and others). Always up to date with the latest API specification.

## Classification

`Open-Source`

![Open-Source](docs/open-source-128x128.png)

---

## Info & Status
[![wunderbon Projects](https://img.shields.io/badge/wunderbon-Projects-green.svg?style=flat)](https://wunderbon.io/) [![wunderbon Projects](https://img.shields.io/badge/license-MIT-green?style=flat)](https://wunderbon.io/) [![wunderbon Projects](https://img.shields.io/badge/wunderbon-Open_Standard-orange?style=flat)](https://wunderbon.io/) ![CircleCI](https://img.shields.io/circleci/build/bitbucket/wunderbon/typescript-declarations/master) [![FOSSA Status](https://app.fossa.com/api/projects/git%2Bbitbucket.org%2Fwunderbon%2Ftypescript-declarations.svg?type=shield)](https://app.fossa.com/projects/git%2Bbitbucket.org%2Fwunderbon%2Ftypescript-declarations?ref=badge_shield)

![Logo of typescript-declarations](https://bitbucket.org/wunderbon/typescript-declarations/raw/master/docs/logo-64x64.png)

---

[![FOSSA Status](https://app.fossa.com/api/projects/git%2Bbitbucket.org%2Fwunderbon%2Ftypescript-declarations.svg?type=large)](https://app.fossa.com/projects/git%2Bbitbucket.org%2Fwunderbon%2Ftypescript-declarations?ref=badge_large)

---

## Table of Contents

- [Features](#features)
- [Requirements](#requirements)
- [Install](#install)
- [Philosophy](#philosophy)
- [Versioning](#versioning)
- [Roadmap](#roadmap)
- [Security-Issues](#security-issues)
- [License »](LICENSE)

## Features

 - Typescript declarations installable via `@types/wunderbon`
 - Clean & well documented

---

## Requirements

 - `Node.js >= 12.0.0` *
 - `npm >= 6.0.0`

---

## Install
You can install these types via:

`npm install @types/wunderbon`

---

## Philosophy

A lot of ❤ for code, documentation & quality. Try it, run it ... love it ;)

---

## Versioning

For a consistent versioning we decided to make use of `Semantic Versioning 2.0.0` http://semver.org. Its easy to understand, very common and known from many other software projects.

---

## Roadmap
- [x] n.a.

---

## Security Issues

If you encounter a (potential) security issue don't hesitate to get in contact with us `security@wunderbon.io` before releasing it to the public. So i get a chance to prepare and release an update before the issue is getting shared. We will honor your work with an Amazon voucher :) - Thank you!

your work with an Amazon voucher :) - Thank you!

---

## Copyright & Licensing

- Icons made by:
  - [Chanut](https://www.flaticon.com/authors/chanut) from [www.flaticon.com](www.flaticon.com)
