// Generated by dts-bundle-generator v4.2.0

/**
 * wunderbon
 *
 * MIT License
 *
 * @copyright 2018 - 2020 wunderbon Operation GmbH & Co. KG - All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/**
 * A receipt representation within the wunderbon domain.
 */
export interface Receipt {
	/**
	 * Barcode
	 */
	barcode?: Barcode;
	/**
	 * The cart contains a collection of cartItems (representation of a product, merchant added
	 * information and the pricing).
	 */
	cart: Cart;
	/**
	 * Information about the cashier at the POS.
	 */
	cashier?: Cashier;
	/**
	 * Payment part of the receipt. This node contains information on how the amount of the cart
	 * was settled.
	 */
	clearing: Clearing;
	/**
	 * Footer lines to be printed on bottom of receipt. Often used to print information about
	 * special deals or a new website.
	 */
	footer?: string[];
	/**
	 * All header lines above the regular merchant and cart information.
	 */
	header?: string[];
	/**
	 * The Id of the location. Each location (branch) of a Merchant has its own Id.
	 */
	location: string;
	/**
	 * Loyalty part of the receipt. Here the user will see e.g. how many 'points' he has earned
	 * or how much money he has saved.
	 */
	loyalty?: Loyalty;
	/**
	 * The Id of the issuer (Merchant). Each Merchant will recive an unique User-Id.
	 */
	merchant: string;
	/**
	 * The Id of the PoS. Each PoS within at the location (branch) has its own Id.
	 */
	pos: string;
}
/**
 * Barcode
 */
export interface Barcode {
	/**
	 * Data for rendering.
	 */
	data: string;
	/**
	 * Format of the barcode.
	 */
	format: Format;
}
/**
 * Format of the barcode.
 */
export declare enum Format {
	Code128 = "Code 128",
	Code39 = "Code 39",
	Ean13UpcA = "EAN-13/UPC-A",
	Ean8 = "EAN-8",
	Itf = "ITF",
	Qr = "QR"
}
/**
 * The cart contains a collection of cartItems (representation of a product, merchant added
 * information and the pricing).
 */
export interface Cart {
	items: CartItem[];
	/**
	 * Taxes overview of items in cart.
	 */
	taxes: TaxOverview[];
	/**
	 * The sum of all items in items.
	 */
	total: CartTotal;
}
/**
 * A cart item representation within the wunderbon domain.
 */
export interface CartItem {
	meta?: ItemMeta;
	/**
	 * Product node either a real EAN/GTIN product or alternatively a generic one.
	 */
	product: Product;
}
export interface ItemMeta {
	/**
	 * Category of this item.
	 */
	category?: Category;
	/**
	 * Group this item belongs to.
	 */
	group?: string;
	/**
	 * Position of this item within its group.
	 */
	groupPosition?: number;
	/**
	 * Position of this item in cart.
	 */
	position?: number;
}
/**
 * Category of this item.
 */
export declare enum Category {
	Food = "food",
	NonFood = "non-food",
	Others = "others"
}
/**
 * Product node either a real EAN/GTIN product or alternatively a generic one.
 */
export interface Product {
	/**
	 * A generic or EAN/GTIN data based product for display.
	 */
	data: Data;
	/**
	 * Price of this cart item.
	 */
	price: Price;
	/**
	 * Quantifier of this cart item (e.g. 1 pcs. or 200 g).
	 */
	quantifier: Quantifier;
}
/**
 * A generic or EAN/GTIN data based product for display.
 *
 * A generic product representation within the wunderbon domain.
 *
 * An EAN product representation within the wunderbon domain.
 */
export interface Data {
	/**
	 * EAN (GTIN) 8||13 product code.
	 */
	code: string;
	/**
	 * A short product name.
	 */
	name?: NameClass | string;
	/**
	 * Product description.
	 */
	description?: string;
	/**
	 * Origin of the product in ISO representation.
	 */
	origin?: Country;
	/**
	 * Supplier or manufacturer of the product.
	 */
	supplier?: string;
}
export interface NameClass {
	/**
	 * Full product name.
	 */
	long?: any;
	/**
	 * Short product name.
	 */
	short: any;
}
/**
 * Origin of the product in ISO representation.
 */
export declare enum Country {
	AE = "AE",
	AF = "AF",
	AI = "AI",
	Ad = "AD",
	Ag = "AG",
	Al = "AL",
	Am = "AM",
	Ao = "AO",
	Aq = "AQ",
	Ar = "AR",
	As = "AS",
	At = "AT",
	Au = "AU",
	Aw = "AW",
	Ax = "AX",
	Az = "AZ",
	BI = "BI",
	BT = "BT",
	BW = "BW",
	Ba = "BA",
	Bb = "BB",
	Bd = "BD",
	Be = "BE",
	Bf = "BF",
	Bg = "BG",
	Bh = "BH",
	Bj = "BJ",
	Bl = "BL",
	Bm = "BM",
	Bn = "BN",
	Bo = "BO",
	Bq = "BQ",
	Br = "BR",
	Bs = "BS",
	Bv = "BV",
	By = "BY",
	Bz = "BZ",
	CA = "CA",
	CD = "CD",
	CF = "CF",
	CG = "CG",
	CM = "CM",
	CN = "CN",
	CR = "CR",
	Cc = "CC",
	Ch = "CH",
	Ci = "CI",
	Ck = "CK",
	Cl = "CL",
	Co = "CO",
	Cu = "CU",
	Cv = "CV",
	Cw = "CW",
	Cx = "CX",
	Cy = "CY",
	Cz = "CZ",
	De = "DE",
	Dj = "DJ",
	Dk = "DK",
	Dm = "DM",
	Do = "DO",
	Dz = "DZ",
	Ec = "EC",
	Ee = "EE",
	Eg = "EG",
	Eh = "EH",
	Er = "ER",
	Es = "ES",
	Et = "ET",
	Fi = "FI",
	Fj = "FJ",
	Fk = "FK",
	Fm = "FM",
	Fo = "FO",
	Fr = "FR",
	GB = "GB",
	Ga = "GA",
	Gd = "GD",
	Ge = "GE",
	Gf = "GF",
	Gg = "GG",
	Gh = "GH",
	Gi = "GI",
	Gl = "GL",
	Gm = "GM",
	Gn = "GN",
	Gp = "GP",
	Gq = "GQ",
	Gr = "GR",
	Gs = "GS",
	Gt = "GT",
	Gu = "GU",
	Gw = "GW",
	Gy = "GY",
	HT = "HT",
	Hk = "HK",
	Hm = "HM",
	Hn = "HN",
	Hr = "HR",
	Hu = "HU",
	ID = "ID",
	IL = "IL",
	IM = "IM",
	IR = "IR",
	Ie = "IE",
	In = "IN",
	Io = "IO",
	Iq = "IQ",
	Is = "IS",
	It = "IT",
	Je = "JE",
	Jm = "JM",
	Jo = "JO",
	Jp = "JP",
	KM = "KM",
	Ke = "KE",
	Kg = "KG",
	Kh = "KH",
	Ki = "KI",
	Kn = "KN",
	Kp = "KP",
	Kr = "KR",
	Kw = "KW",
	Ky = "KY",
	Kz = "KZ",
	LB = "LB",
	LV = "LV",
	La = "LA",
	Lc = "LC",
	Li = "LI",
	Lk = "LK",
	Lr = "LR",
	Ls = "LS",
	Lt = "LT",
	Lu = "LU",
	Ly = "LY",
	MF = "MF",
	MS = "MS",
	MT = "MT",
	MX = "MX",
	Ma = "MA",
	Mc = "MC",
	Md = "MD",
	Me = "ME",
	Mg = "MG",
	Mh = "MH",
	Mk = "MK",
	Ml = "ML",
	Mm = "MM",
	Mn = "MN",
	Mo = "MO",
	Mp = "MP",
	Mq = "MQ",
	Mr = "MR",
	Mu = "MU",
	Mv = "MV",
	Mw = "MW",
	My = "MY",
	Mz = "MZ",
	NI = "NI",
	NP = "NP",
	Na = "NA",
	Nc = "NC",
	Ne = "NE",
	Nf = "NF",
	Ng = "NG",
	Nl = "NL",
	No = "NO",
	Nr = "NR",
	Nu = "NU",
	Nz = "NZ",
	Om = "OM",
	PE = "PE",
	PG = "PG",
	PR = "PR",
	PS = "PS",
	Pa = "PA",
	Pf = "PF",
	Ph = "PH",
	Pk = "PK",
	Pl = "PL",
	Pm = "PM",
	Pn = "PN",
	Pt = "PT",
	Pw = "PW",
	Py = "PY",
	QA = "QA",
	Re = "RE",
	Ro = "RO",
	Rs = "RS",
	Ru = "RU",
	Rw = "RW",
	SD = "SD",
	SE = "SE",
	Sa = "SA",
	Sb = "SB",
	Sc = "SC",
	Sg = "SG",
	Sh = "SH",
	Si = "SI",
	Sj = "SJ",
	Sk = "SK",
	Sl = "SL",
	Sm = "SM",
	Sn = "SN",
	So = "SO",
	Sr = "SR",
	Ss = "SS",
	St = "ST",
	Sv = "SV",
	Sx = "SX",
	Sy = "SY",
	Sz = "SZ",
	Tc = "TC",
	Td = "TD",
	Tf = "TF",
	Tg = "TG",
	Th = "TH",
	Tj = "TJ",
	Tk = "TK",
	Tl = "TL",
	Tm = "TM",
	Tn = "TN",
	To = "TO",
	Tr = "TR",
	Tt = "TT",
	Tv = "TV",
	Tw = "TW",
	Tz = "TZ",
	Ua = "UA",
	Ug = "UG",
	Um = "UM",
	Us = "US",
	Uy = "UY",
	Uz = "UZ",
	VG = "VG",
	Va = "VA",
	Vc = "VC",
	Ve = "VE",
	Vi = "VI",
	Vn = "VN",
	Vu = "VU",
	Wf = "WF",
	Ws = "WS",
	Ye = "YE",
	Yt = "YT",
	Za = "ZA",
	Zm = "ZM",
	Zw = "ZW"
}
/**
 * Price of this cart item.
 */
export interface Price {
	/**
	 * Price INCLUDING tax (e.g. VAT = Value Added Tax or others).
	 */
	gross: PriceGross;
	/**
	 * Price EXCLUDING tax (e.g. VAT = Value Added Tax or others).
	 */
	net: PriceNet;
	/**
	 * Taxes applied on this item (e.g. VAT = Value Added Tax or others).
	 */
	tax: PriceTax;
}
/**
 * Price INCLUDING tax (e.g. VAT = Value Added Tax or others).
 */
export interface PriceGross {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * Currency of amount.
 *
 * Currency of exchange rate.
 */
export declare enum CurrencyEnum {
	AMD = "AMD",
	Aed = "AED",
	Afn = "AFN",
	All = "ALL",
	Ang = "ANG",
	Aoa = "AOA",
	Ars = "ARS",
	Aud = "AUD",
	Awg = "AWG",
	Azn = "AZN",
	BAM = "BAM",
	BSD = "BSD",
	Bbd = "BBD",
	Bdt = "BDT",
	Bgn = "BGN",
	Bhd = "BHD",
	Bif = "BIF",
	Bmd = "BMD",
	Bnd = "BND",
	Bob = "BOB",
	Bov = "BOV",
	Brl = "BRL",
	Btn = "BTN",
	Bwp = "BWP",
	Byr = "BYR",
	Bzd = "BZD",
	CAD = "CAD",
	CRC = "CRC",
	Cdf = "CDF",
	Che = "CHE",
	Chf = "CHF",
	Chw = "CHW",
	Clf = "CLF",
	Clp = "CLP",
	Cny = "CNY",
	Cop = "COP",
	Cou = "COU",
	Cuc = "CUC",
	Cup = "CUP",
	Cve = "CVE",
	Czk = "CZK",
	Djf = "DJF",
	Dkk = "DKK",
	Dop = "DOP",
	Dzd = "DZD",
	EGP = "EGP",
	Ern = "ERN",
	Etb = "ETB",
	Eur = "EUR",
	Fjd = "FJD",
	Fkp = "FKP",
	Gbp = "GBP",
	Gel = "GEL",
	Ghs = "GHS",
	Gip = "GIP",
	Gmd = "GMD",
	Gnf = "GNF",
	Gtq = "GTQ",
	Gyd = "GYD",
	Hkd = "HKD",
	Hnl = "HNL",
	Hrk = "HRK",
	Htg = "HTG",
	Huf = "HUF",
	Idr = "IDR",
	Ils = "ILS",
	Inr = "INR",
	Iqd = "IQD",
	Irr = "IRR",
	Isk = "ISK",
	Jmd = "JMD",
	Jod = "JOD",
	Jpy = "JPY",
	Kes = "KES",
	Kgs = "KGS",
	Khr = "KHR",
	Kmf = "KMF",
	Kpw = "KPW",
	Krw = "KRW",
	Kwd = "KWD",
	Kyd = "KYD",
	Kzt = "KZT",
	Lak = "LAK",
	Lbp = "LBP",
	Lkr = "LKR",
	Lrd = "LRD",
	Lsl = "LSL",
	Lyd = "LYD",
	Mad = "MAD",
	Mdl = "MDL",
	Mga = "MGA",
	Mkd = "MKD",
	Mmk = "MMK",
	Mnt = "MNT",
	Mop = "MOP",
	Mro = "MRO",
	Mur = "MUR",
	Mvr = "MVR",
	Mwk = "MWK",
	Mxn = "MXN",
	Mxv = "MXV",
	Myr = "MYR",
	Mzn = "MZN",
	NIO = "NIO",
	Nad = "NAD",
	Ngn = "NGN",
	Nok = "NOK",
	Npr = "NPR",
	Nzd = "NZD",
	OMR = "OMR",
	PHP = "PHP",
	Pab = "PAB",
	Pen = "PEN",
	Pgk = "PGK",
	Pkr = "PKR",
	Pln = "PLN",
	Pyg = "PYG",
	Qar = "QAR",
	Ron = "RON",
	Rsd = "RSD",
	Rub = "RUB",
	Rwf = "RWF",
	SSP = "SSP",
	SVC = "SVC",
	Sar = "SAR",
	Sbd = "SBD",
	Scr = "SCR",
	Sdg = "SDG",
	Sek = "SEK",
	Sgd = "SGD",
	Shp = "SHP",
	Sll = "SLL",
	Sos = "SOS",
	Srd = "SRD",
	Std = "STD",
	Syp = "SYP",
	Szl = "SZL",
	Thb = "THB",
	Tjs = "TJS",
	Tmt = "TMT",
	Tnd = "TND",
	Top = "TOP",
	Try = "TRY",
	Ttd = "TTD",
	Twd = "TWD",
	Tzs = "TZS",
	Uah = "UAH",
	Ugx = "UGX",
	Usd = "USD",
	Usn = "USN",
	Uyi = "UYI",
	Uyu = "UYU",
	Uzs = "UZS",
	Vef = "VEF",
	Vnd = "VND",
	Vuv = "VUV",
	Wst = "WST",
	XAG = "XAG",
	Xaf = "XAF",
	Xau = "XAU",
	Xba = "XBA",
	Xbb = "XBB",
	Xbc = "XBC",
	Xbd = "XBD",
	Xcd = "XCD",
	Xdr = "XDR",
	Xof = "XOF",
	Xpd = "XPD",
	Xpf = "XPF",
	Xpt = "XPT",
	Xsu = "XSU",
	Xts = "XTS",
	Xua = "XUA",
	Xxx = "XXX",
	Yer = "YER",
	Zar = "ZAR",
	Zmw = "ZMW",
	Zwl = "ZWL"
}
/**
 * Price EXCLUDING tax (e.g. VAT = Value Added Tax or others).
 */
export interface PriceNet {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * Taxes applied on this item (e.g. VAT = Value Added Tax or others).
 */
export interface PriceTax {
	/**
	 * An amount representation within the wunderbon domain.
	 */
	amount: PurpleAmount;
	/**
	 * Percentage of tax.
	 */
	percentage: number;
	/**
	 * A symbol as abbreviation in overview.
	 */
	symbol?: string;
	/**
	 * Type of tax e.g. 'VAT'
	 */
	type: TaxType;
}
/**
 * An amount representation within the wunderbon domain.
 */
export interface PurpleAmount {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * Type of tax e.g. 'VAT'
 */
export declare enum TaxType {
	Gst = "GST",
	Vat = "VAT"
}
/**
 * Quantifier of this cart item (e.g. 1 pcs. or 200 g).
 */
export interface Quantifier {
	/**
	 * Amount of this item in cart.
	 */
	quantity: number;
	/**
	 * Unit of the quantifier (e.g. kg, g, mm, m, pieces, ...).
	 */
	unit: Unit;
}
/**
 * Unit of the quantifier (e.g. kg, g, mm, m, pieces, ...).
 */
export declare enum Unit {
	CM = "cm",
	Dm = "dm",
	Ft = "ft",
	G = "g",
	Gt = "Gt",
	In = "in",
	KM = "km",
	Kg = "kg",
	M = "m",
	MT = "Mt",
	Mg = "mg",
	Mi = "mi",
	Mm = "mm",
	Ng = "ng",
	PG = "pg",
	Pcs = "pcs",
	Sm = "sm",
	T = "t",
	Yd = "yd",
	Μg = "\u00B5g"
}
/**
 * A tax overview representation within the wunderbon domain.
 */
export interface TaxOverview {
	/**
	 * The gross amount.
	 */
	gross: TaxGross;
	/**
	 * The net amount.
	 */
	net: TaxNet;
	/**
	 * The symbol as index for the overview line (e.g. "A" or "1").
	 */
	symbol: string;
	/**
	 * The tax properties for overview line.
	 */
	tax: TaxTax;
}
/**
 * The gross amount.
 */
export interface TaxGross {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * The net amount.
 */
export interface TaxNet {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * The tax properties for overview line.
 */
export interface TaxTax {
	/**
	 * An amount representation within the wunderbon domain.
	 */
	amount: FluffyAmount;
	/**
	 * Percentage of tax.
	 */
	percentage: number;
	/**
	 * A symbol as abbreviation in overview.
	 */
	symbol?: string;
	/**
	 * Type of tax e.g. 'VAT'
	 */
	type: TaxType;
}
/**
 * An amount representation within the wunderbon domain.
 */
export interface FluffyAmount {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * The sum of all items in items.
 */
export interface CartTotal {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * Information about the cashier at the POS.
 */
export interface Cashier {
	/**
	 * Name of the cashier.
	 */
	name: PrivateName | string;
}
/**
 * A schema for private persons name representation within the wunderbon domain.
 */
export interface PrivateName {
	firstName: string;
	lastName: string;
	salutation?: Salutation;
}
export declare enum Salutation {
	MS = "Ms",
	Mr = "Mr",
	Mrs = "Mrs"
}
/**
 * Payment part of the receipt. This node contains information on how the amount of the cart
 * was settled.
 */
export interface Clearing {
	/**
	 * Collection of payments applied. Must contain at least one payment.
	 */
	payments: CashPayment[];
	/**
	 * The sum of all payments cleared.
	 */
	total: ClearingTotal;
}
/**
 * A cash payment representation within the wunderbon domain.
 *
 * A cashless payment representation within the wunderbon domain.
 */
export interface CashPayment {
	/**
	 * Change for customer.
	 */
	change?: ChangeClass;
	/**
	 * A description of the payment. Can be used for example for persisting a voucher code.
	 */
	description?: string;
	/**
	 * Amount handed over for settling.
	 */
	given: GivenClass;
	/**
	 * Sum of prices of all products in cart.
	 */
	total: PaymentTotal;
	/**
	 * Meta data of transaction from terminal.
	 */
	meta?: PaymentMeta;
}
/**
 * Change for customer.
 */
export interface ChangeClass {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * Amount handed over for settling.
 */
export interface GivenClass {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * Meta data of transaction from terminal.
 */
export interface PaymentMeta {
	/**
	 * Transaction data from terminal.
	 */
	data: TerminalRed;
	/**
	 * The cashless payment type. Aligned to w3 (@see
	 * https://www.w3.org/TR/payment-method-id/#registry).
	 */
	type: MetaType;
}
/**
 * Transaction data from terminal.
 *
 * A buffered terminal response representation within the wunderbon domain.
 *
 * A structured terminal response representation within the wunderbon domain.
 */
export interface TerminalRed {
	/**
	 * A collection of buffered strings for display.
	 */
	buffer?: string[];
	/**
	 * The card sequence number.
	 */
	cardSequenceNumber?: string;
	/**
	 * Type of card used.
	 */
	cardType?: CardType;
	/**
	 * Flag for contactless payments.
	 */
	contactless?: boolean;
	/**
	 * The VU-Nr. of the transaction (https://www.novalnet.de/payment-lexikon/vu-nummer).
	 */
	contractingCompanyNumber?: string;
	/**
	 * Date of the transaction (ISO 8601 YYYY-MM-TT).
	 */
	date?: string;
	/**
	 * The EMV-AID of the transaction
	 * (https://www.eftlab.com/knowledge-base/211-emv-aid-rid-pix/).
	 */
	emvApplicationIdentifier?: string;
	/**
	 * Additional buffered lines from EMV.
	 */
	emvDataBuffer?: string[];
	/**
	 * The number of the receipt.
	 */
	receiptNumber?: string;
	/**
	 * The Id of the terminal.
	 */
	terminalId?: string;
	/**
	 * Time of the transaction (e.g. 12:00:00).
	 */
	time?: string;
	/**
	 * The title of the sales slip.
	 */
	title?: string;
	/**
	 * Total amount of transaction.
	 */
	total?: DataTotal;
	/**
	 * The number of the transaction.
	 */
	transactionNumber?: string;
	/**
	 * The PAN of the card used for transaction
	 * (https://de.wikipedia.org/wiki/ISO/IEC_7810#Track_#_2).
	 */
	truncatedPan?: TruncatedPanClass;
}
/**
 * Type of card used.
 */
export declare enum CardType {
	DebitMastercard = "Debit Mastercard",
	EuroELV = "EuroELV",
	Girocard = "Girocard",
	SEPALastschrift = "SEPA-Lastschrift",
	SepaElv = "SEPA ELV"
}
/**
 * Total amount of transaction.
 */
export interface DataTotal {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * The PAN of the card used for transaction
 * (https://de.wikipedia.org/wiki/ISO/IEC_7810#Track_#_2).
 */
export interface TruncatedPanClass {
	/**
	 * The symbol used as asterisk for truncation.
	 */
	asterisks: any;
	/**
	 * The truncated number.
	 */
	number: string;
}
/**
 * The cashless payment type. Aligned to w3 (@see
 * https://www.w3.org/TR/payment-method-id/#registry).
 */
export declare enum MetaType {
	BasicCard = "basic-card"
}
/**
 * Sum of prices of all products in cart.
 */
export interface PaymentTotal {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * The sum of all payments cleared.
 */
export interface ClearingTotal {
	/**
	 * Currency of amount.
	 */
	currency: CurrencyEnum;
	/**
	 * The value of the amount.
	 */
	value: number;
}
/**
 * Loyalty part of the receipt. Here the user will see e.g. how many 'points' he has earned
 * or how much money he has saved.
 */
export interface Loyalty {
	/**
	 * The loyalty information
	 */
	data: LoyaltyRed;
}
/**
 * The loyalty information
 *
 * A buffered loyalty representation within the wunderbon domain.
 *
 * A structured loyalty representation within the wunderbon domain.
 */
export interface LoyaltyRed {
	/**
	 * A collection of lines for display.
	 */
	buffer?: string[];
	/**
	 * The current balance before and after current transaction.
	 */
	balance?: Balance;
	/**
	 * The 'currency' of the program.
	 */
	currency?: Currency;
	/**
	 * The identifier (e.g. Customer-Nr.) within this program.
	 */
	identifier?: IdentifierClass;
	/**
	 * The name of the loyalty program (e.g. PAYBACK or DeutschlandCard).
	 */
	name?: string;
}
/**
 * The current balance before and after current transaction.
 */
export interface Balance {
	/**
	 * Exchange rate description.
	 */
	exchange?: Exchange;
	/**
	 * The balance after the current transaction.
	 */
	is: string;
	/**
	 * The balance before the current transaction.
	 */
	was: string;
}
/**
 * Exchange rate description.
 */
export interface Exchange {
	/**
	 * Exchange rates.
	 */
	rates?: Rate[];
}
export interface Rate {
	/**
	 * Currency of exchange rate.
	 */
	currency?: CurrencyEnum;
	/**
	 * Rate for exchange.
	 */
	rate?: number;
}
/**
 * The 'currency' of the program.
 */
export interface Currency {
	/**
	 * The name of the currency (e.g. PAYBACK Punkte).
	 */
	name: string;
}
/**
 * The identifier (e.g. Customer-Nr.) within this program.
 */
export interface IdentifierClass {
	/**
	 * The symbol used as asterisk for truncation.
	 */
	asterisks: any;
	/**
	 * The truncated number.
	 */
	number: string;
}
export declare class Convert {
	static toReceipt(json: string): Receipt;
	static receiptToJson(value: Receipt): string;
}

export {};
