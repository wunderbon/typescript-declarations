
/**
 * wunderbon
 *
 * MIT License
 *
 * @copyright 2018 - 2020 wunderbon Operation GmbH & Co. KG - All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// To parse this data:
//
//   import { Convert, Location } from "./file";
//
//   const location = Convert.toLocation(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

/**
 * Location representation within the wunderbon domain.
 */
export interface Location {
    /**
     * Address of the location.
     */
    address: Address;
    /**
     * Communication setup of the location.
     */
    communication?: Communication;
    /**
     * Name of the location.
     */
    name?: LegalName;
    /**
     * Information about the POS' of this location.
     */
    posData: POSData;
    /**
     * Service times of this location
     */
    serviceTime?: ServiceTime[];
    /**
     * Tax-Id of this location (only required if for example every location acts on their own
     * behalf e.g. own registered company). If not passed the Tax-Id of Merchant is used.
     */
    taxId?: string;
}

/**
 * Address of the location.
 */
export interface Address {
    /**
     * City of the address.
     */
    city: string;
    /**
     * Country of the address.
     */
    country: Country;
    /**
     * GPS coordinates of the address.
     */
    gps?: AddressGps;
    /**
     * Street and Number as well as address additions.
     */
    street: Street;
    /**
     * ZIP Code of the address.
     */
    zip: string;
}

/**
 * Country of the address.
 */
export enum Country {
    AE = "AE",
    AF = "AF",
    AI = "AI",
    Ad = "AD",
    Ag = "AG",
    Al = "AL",
    Am = "AM",
    Ao = "AO",
    Aq = "AQ",
    Ar = "AR",
    As = "AS",
    At = "AT",
    Au = "AU",
    Aw = "AW",
    Ax = "AX",
    Az = "AZ",
    BI = "BI",
    BT = "BT",
    BW = "BW",
    Ba = "BA",
    Bb = "BB",
    Bd = "BD",
    Be = "BE",
    Bf = "BF",
    Bg = "BG",
    Bh = "BH",
    Bj = "BJ",
    Bl = "BL",
    Bm = "BM",
    Bn = "BN",
    Bo = "BO",
    Bq = "BQ",
    Br = "BR",
    Bs = "BS",
    Bv = "BV",
    By = "BY",
    Bz = "BZ",
    CA = "CA",
    CD = "CD",
    CF = "CF",
    CG = "CG",
    CM = "CM",
    CN = "CN",
    CR = "CR",
    Cc = "CC",
    Ch = "CH",
    Ci = "CI",
    Ck = "CK",
    Cl = "CL",
    Co = "CO",
    Cu = "CU",
    Cv = "CV",
    Cw = "CW",
    Cx = "CX",
    Cy = "CY",
    Cz = "CZ",
    De = "DE",
    Dj = "DJ",
    Dk = "DK",
    Dm = "DM",
    Do = "DO",
    Dz = "DZ",
    Ec = "EC",
    Ee = "EE",
    Eg = "EG",
    Eh = "EH",
    Er = "ER",
    Es = "ES",
    Et = "ET",
    Fi = "FI",
    Fj = "FJ",
    Fk = "FK",
    Fm = "FM",
    Fo = "FO",
    Fr = "FR",
    GB = "GB",
    Ga = "GA",
    Gd = "GD",
    Ge = "GE",
    Gf = "GF",
    Gg = "GG",
    Gh = "GH",
    Gi = "GI",
    Gl = "GL",
    Gm = "GM",
    Gn = "GN",
    Gp = "GP",
    Gq = "GQ",
    Gr = "GR",
    Gs = "GS",
    Gt = "GT",
    Gu = "GU",
    Gw = "GW",
    Gy = "GY",
    HT = "HT",
    Hk = "HK",
    Hm = "HM",
    Hn = "HN",
    Hr = "HR",
    Hu = "HU",
    ID = "ID",
    IL = "IL",
    IM = "IM",
    IR = "IR",
    Ie = "IE",
    In = "IN",
    Io = "IO",
    Iq = "IQ",
    Is = "IS",
    It = "IT",
    Je = "JE",
    Jm = "JM",
    Jo = "JO",
    Jp = "JP",
    KM = "KM",
    Ke = "KE",
    Kg = "KG",
    Kh = "KH",
    Ki = "KI",
    Kn = "KN",
    Kp = "KP",
    Kr = "KR",
    Kw = "KW",
    Ky = "KY",
    Kz = "KZ",
    LB = "LB",
    LV = "LV",
    La = "LA",
    Lc = "LC",
    Li = "LI",
    Lk = "LK",
    Lr = "LR",
    Ls = "LS",
    Lt = "LT",
    Lu = "LU",
    Ly = "LY",
    MF = "MF",
    MS = "MS",
    MT = "MT",
    MX = "MX",
    Ma = "MA",
    Mc = "MC",
    Md = "MD",
    Me = "ME",
    Mg = "MG",
    Mh = "MH",
    Mk = "MK",
    Ml = "ML",
    Mm = "MM",
    Mn = "MN",
    Mo = "MO",
    Mp = "MP",
    Mq = "MQ",
    Mr = "MR",
    Mu = "MU",
    Mv = "MV",
    Mw = "MW",
    My = "MY",
    Mz = "MZ",
    NI = "NI",
    NP = "NP",
    Na = "NA",
    Nc = "NC",
    Ne = "NE",
    Nf = "NF",
    Ng = "NG",
    Nl = "NL",
    No = "NO",
    Nr = "NR",
    Nu = "NU",
    Nz = "NZ",
    Om = "OM",
    PE = "PE",
    PG = "PG",
    PR = "PR",
    PS = "PS",
    Pa = "PA",
    Pf = "PF",
    Ph = "PH",
    Pk = "PK",
    Pl = "PL",
    Pm = "PM",
    Pn = "PN",
    Pt = "PT",
    Pw = "PW",
    Py = "PY",
    QA = "QA",
    Re = "RE",
    Ro = "RO",
    Rs = "RS",
    Ru = "RU",
    Rw = "RW",
    SD = "SD",
    SE = "SE",
    Sa = "SA",
    Sb = "SB",
    Sc = "SC",
    Sg = "SG",
    Sh = "SH",
    Si = "SI",
    Sj = "SJ",
    Sk = "SK",
    Sl = "SL",
    Sm = "SM",
    Sn = "SN",
    So = "SO",
    Sr = "SR",
    Ss = "SS",
    St = "ST",
    Sv = "SV",
    Sx = "SX",
    Sy = "SY",
    Sz = "SZ",
    Tc = "TC",
    Td = "TD",
    Tf = "TF",
    Tg = "TG",
    Th = "TH",
    Tj = "TJ",
    Tk = "TK",
    Tl = "TL",
    Tm = "TM",
    Tn = "TN",
    To = "TO",
    Tr = "TR",
    Tt = "TT",
    Tv = "TV",
    Tw = "TW",
    Tz = "TZ",
    Ua = "UA",
    Ug = "UG",
    Um = "UM",
    Us = "US",
    Uy = "UY",
    Uz = "UZ",
    VG = "VG",
    Va = "VA",
    Vc = "VC",
    Ve = "VE",
    Vi = "VI",
    Vn = "VN",
    Vu = "VU",
    Wf = "WF",
    Ws = "WS",
    Ye = "YE",
    Yt = "YT",
    Za = "ZA",
    Zm = "ZM",
    Zw = "ZW",
}

/**
 * GPS coordinates of the address.
 */
export interface AddressGps {
    /**
     * Latitude of the GPS coordinate.
     */
    latitude: number;
    /**
     * Longitude of the GPS coordinate.
     */
    longitude: number;
}

/**
 * Street and Number as well as address additions.
 */
export interface Street {
    /**
     * Any additions like 'Apartment No.1'.
     */
    additional?: string;
    /**
     * Name of the street.
     */
    name: string;
    /**
     * Corresponding house number.
     */
    number: number;
}

/**
 * Communication setup of the location.
 */
export interface Communication {
    /**
     * Email address.
     */
    email?: string;
    /**
     * Fax number.
     */
    fax?: FaxElement[];
    /**
     * Phone number.
     */
    phone?: PhoneElement[];
    /**
     * Internet address.
     */
    www?: string;
}

/**
 * Phone-Number representation within the wunderbon domain.
 */
export interface FaxElement {
    areaCode: FaxAreaCode;
    name?:    string;
    number:   string;
}

export interface FaxAreaCode {
    country: string;
    local:   string;
}

/**
 * Phone-Number representation within the wunderbon domain.
 */
export interface PhoneElement {
    areaCode: PhoneAreaCode;
    name?:    string;
    number:   string;
}

export interface PhoneAreaCode {
    country: string;
    local:   string;
}

/**
 * Name of the location.
 */
export interface LegalName {
    displayName?: string;
    entityName:   string;
}

/**
 * Information about the POS' of this location.
 */
export interface POSData {
    posList: Pos[];
}

/**
 * POS representation within the wunderbon domain.
 */
export interface Pos {
    /**
     * Data of the POS.
     */
    data: DataClass;
    /**
     * The Id of the PoS. Each PoS within at the location (branch) has its own Id.
     */
    id: string;
    /**
     * Type of the POS.
     */
    type: Type;
}

/**
 * Data of the POS.
 *
 * POS inside a building representation within the wunderbon domain.
 *
 * POS outside a building representation within the wunderbon domain.
 *
 * POS virtual representation within the wunderbon domain.
 */
export interface DataClass {
    /**
     * Building the POS is located in.
     */
    building?: string;
    /**
     * Floor the POS is located at.
     */
    floor?: number;
    /**
     * GPS coordinates of the POS.
     */
    gps?: DataGps;
    /**
     * Number of the POS in a row (e.g. 1 or 3 or ...).
     */
    number?: number;
    /**
     * Room the POS is located in.
     */
    room?: string;
    /**
     * Internet address (URL) of the virtual POS (best link is into carts checkout).
     */
    url?: string;
}

/**
 * GPS coordinates of the POS.
 */
export interface DataGps {
    /**
     * Latitude of the GPS coordinate.
     */
    latitude: number;
    /**
     * Longitude of the GPS coordinate.
     */
    longitude: number;
}

/**
 * Type of the POS.
 */
export enum Type {
    Inside = "inside",
    Online = "online",
    Outside = "outside",
}

/**
 * Service-Time representation within the wunderbon domain.
 */
export interface ServiceTime {
    /**
     * Time window begin.
     */
    from: From;
    /**
     * Time window end.
     */
    to: To;
    /**
     * Day of the week.
     */
    weekday: Weekday;
}

/**
 * Time window begin.
 */
export interface From {
    /**
     * The hour where the time window begins.
     */
    hour: number;
    /**
     * The minute where the time window ends.
     */
    minute: number;
}

/**
 * Time window end.
 */
export interface To {
    /**
     * The hour where the tme window ends.
     */
    hour: number;
    /**
     * The minute where the time window ends.
     */
    minute: number;
}

/**
 * Day of the week.
 */
export enum Weekday {
    Friday = "Friday",
    Monday = "Monday",
    Saturday = "Saturday",
    Sunday = "Sunday",
    Thursday = "Thursday",
    Tuesday = "Tuesday",
    Wednesday = "Wednesday",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
    public static toLocation(json: string): Location {
        return cast(JSON.parse(json), r("Location"));
    }

    public static locationToJson(value: Location): string {
        return JSON.stringify(uncast(value, r("Location")), null, 2);
    }
}

function invalidValue(typ: any, val: any): never {
    throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`);
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        var l = typs.length;
        for (var i = 0; i < l; i++) {
            var typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases, val);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue("array", val);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(typ: any, val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue("Date", val);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue("object", val);
        }
        var result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val);
    }
    if (typ === false) return invalidValue(typ, val);
    while (typeof typ === "object" && typ.ref !== undefined) {
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
            : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
            : invalidValue(typ, val);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(typ, val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

function m(additional: any) {
    return { props: [], additional };
}

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "Location": o([
        { json: "address", js: "address", typ: r("Address") },
        { json: "communication", js: "communication", typ: u(undefined, r("Communication")) },
        { json: "name", js: "name", typ: u(undefined, r("LegalName")) },
        { json: "posData", js: "posData", typ: r("POSData") },
        { json: "serviceTime", js: "serviceTime", typ: u(undefined, a(r("ServiceTime"))) },
        { json: "taxId", js: "taxId", typ: u(undefined, "") },
    ], false),
    "Address": o([
        { json: "city", js: "city", typ: "" },
        { json: "country", js: "country", typ: r("Country") },
        { json: "gps", js: "gps", typ: u(undefined, r("AddressGps")) },
        { json: "street", js: "street", typ: r("Street") },
        { json: "zip", js: "zip", typ: "" },
    ], false),
    "AddressGps": o([
        { json: "latitude", js: "latitude", typ: 3.14 },
        { json: "longitude", js: "longitude", typ: 3.14 },
    ], false),
    "Street": o([
        { json: "additional", js: "additional", typ: u(undefined, "") },
        { json: "name", js: "name", typ: "" },
        { json: "number", js: "number", typ: 0 },
    ], false),
    "Communication": o([
        { json: "email", js: "email", typ: u(undefined, "") },
        { json: "fax", js: "fax", typ: u(undefined, a(r("FaxElement"))) },
        { json: "phone", js: "phone", typ: u(undefined, a(r("PhoneElement"))) },
        { json: "www", js: "www", typ: u(undefined, "") },
    ], false),
    "FaxElement": o([
        { json: "areaCode", js: "areaCode", typ: r("FaxAreaCode") },
        { json: "name", js: "name", typ: u(undefined, "") },
        { json: "number", js: "number", typ: "" },
    ], false),
    "FaxAreaCode": o([
        { json: "country", js: "country", typ: "" },
        { json: "local", js: "local", typ: "" },
    ], false),
    "PhoneElement": o([
        { json: "areaCode", js: "areaCode", typ: r("PhoneAreaCode") },
        { json: "name", js: "name", typ: u(undefined, "") },
        { json: "number", js: "number", typ: "" },
    ], false),
    "PhoneAreaCode": o([
        { json: "country", js: "country", typ: "" },
        { json: "local", js: "local", typ: "" },
    ], false),
    "LegalName": o([
        { json: "displayName", js: "displayName", typ: u(undefined, "") },
        { json: "entityName", js: "entityName", typ: "" },
    ], false),
    "POSData": o([
        { json: "posList", js: "posList", typ: a(r("Pos")) },
    ], false),
    "Pos": o([
        { json: "data", js: "data", typ: r("DataClass") },
        { json: "id", js: "id", typ: "" },
        { json: "type", js: "type", typ: r("Type") },
    ], false),
    "DataClass": o([
        { json: "building", js: "building", typ: u(undefined, "") },
        { json: "floor", js: "floor", typ: u(undefined, 0) },
        { json: "gps", js: "gps", typ: u(undefined, r("DataGps")) },
        { json: "number", js: "number", typ: u(undefined, 0) },
        { json: "room", js: "room", typ: u(undefined, "") },
        { json: "url", js: "url", typ: u(undefined, "") },
    ], false),
    "DataGps": o([
        { json: "latitude", js: "latitude", typ: 3.14 },
        { json: "longitude", js: "longitude", typ: 3.14 },
    ], false),
    "ServiceTime": o([
        { json: "from", js: "from", typ: r("From") },
        { json: "to", js: "to", typ: r("To") },
        { json: "weekday", js: "weekday", typ: r("Weekday") },
    ], false),
    "From": o([
        { json: "hour", js: "hour", typ: 0 },
        { json: "minute", js: "minute", typ: 0 },
    ], false),
    "To": o([
        { json: "hour", js: "hour", typ: 0 },
        { json: "minute", js: "minute", typ: 0 },
    ], false),
    "Country": [
        "AE",
        "AF",
        "AI",
        "AD",
        "AG",
        "AL",
        "AM",
        "AO",
        "AQ",
        "AR",
        "AS",
        "AT",
        "AU",
        "AW",
        "AX",
        "AZ",
        "BI",
        "BT",
        "BW",
        "BA",
        "BB",
        "BD",
        "BE",
        "BF",
        "BG",
        "BH",
        "BJ",
        "BL",
        "BM",
        "BN",
        "BO",
        "BQ",
        "BR",
        "BS",
        "BV",
        "BY",
        "BZ",
        "CA",
        "CD",
        "CF",
        "CG",
        "CM",
        "CN",
        "CR",
        "CC",
        "CH",
        "CI",
        "CK",
        "CL",
        "CO",
        "CU",
        "CV",
        "CW",
        "CX",
        "CY",
        "CZ",
        "DE",
        "DJ",
        "DK",
        "DM",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "EH",
        "ER",
        "ES",
        "ET",
        "FI",
        "FJ",
        "FK",
        "FM",
        "FO",
        "FR",
        "GB",
        "GA",
        "GD",
        "GE",
        "GF",
        "GG",
        "GH",
        "GI",
        "GL",
        "GM",
        "GN",
        "GP",
        "GQ",
        "GR",
        "GS",
        "GT",
        "GU",
        "GW",
        "GY",
        "HT",
        "HK",
        "HM",
        "HN",
        "HR",
        "HU",
        "ID",
        "IL",
        "IM",
        "IR",
        "IE",
        "IN",
        "IO",
        "IQ",
        "IS",
        "IT",
        "JE",
        "JM",
        "JO",
        "JP",
        "KM",
        "KE",
        "KG",
        "KH",
        "KI",
        "KN",
        "KP",
        "KR",
        "KW",
        "KY",
        "KZ",
        "LB",
        "LV",
        "LA",
        "LC",
        "LI",
        "LK",
        "LR",
        "LS",
        "LT",
        "LU",
        "LY",
        "MF",
        "MS",
        "MT",
        "MX",
        "MA",
        "MC",
        "MD",
        "ME",
        "MG",
        "MH",
        "MK",
        "ML",
        "MM",
        "MN",
        "MO",
        "MP",
        "MQ",
        "MR",
        "MU",
        "MV",
        "MW",
        "MY",
        "MZ",
        "NI",
        "NP",
        "NA",
        "NC",
        "NE",
        "NF",
        "NG",
        "NL",
        "NO",
        "NR",
        "NU",
        "NZ",
        "OM",
        "PE",
        "PG",
        "PR",
        "PS",
        "PA",
        "PF",
        "PH",
        "PK",
        "PL",
        "PM",
        "PN",
        "PT",
        "PW",
        "PY",
        "QA",
        "RE",
        "RO",
        "RS",
        "RU",
        "RW",
        "SD",
        "SE",
        "SA",
        "SB",
        "SC",
        "SG",
        "SH",
        "SI",
        "SJ",
        "SK",
        "SL",
        "SM",
        "SN",
        "SO",
        "SR",
        "SS",
        "ST",
        "SV",
        "SX",
        "SY",
        "SZ",
        "TC",
        "TD",
        "TF",
        "TG",
        "TH",
        "TJ",
        "TK",
        "TL",
        "TM",
        "TN",
        "TO",
        "TR",
        "TT",
        "TV",
        "TW",
        "TZ",
        "UA",
        "UG",
        "UM",
        "US",
        "UY",
        "UZ",
        "VG",
        "VA",
        "VC",
        "VE",
        "VI",
        "VN",
        "VU",
        "WF",
        "WS",
        "YE",
        "YT",
        "ZA",
        "ZM",
        "ZW",
    ],
    "Type": [
        "inside",
        "online",
        "outside",
    ],
    "Weekday": [
        "Friday",
        "Monday",
        "Saturday",
        "Sunday",
        "Thursday",
        "Tuesday",
        "Wednesday",
    ],
};
