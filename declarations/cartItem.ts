
/**
 * wunderbon
 *
 * MIT License
 *
 * @copyright 2018 - 2020 wunderbon Operation GmbH & Co. KG - All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

// To parse this data:
//
//   import { Convert, CartItem } from "./file";
//
//   const cartItem = Convert.toCartItem(json);
//
// These functions will throw an error if the JSON doesn't
// match the expected interface, even if the JSON is valid.

/**
 * A cart item representation within the wunderbon domain.
 */
export interface CartItem {
    meta?: Meta;
    /**
     * Product node either a real EAN/GTIN product or alternatively a generic one.
     */
    product: Product;
}

export interface Meta {
    /**
     * Category of this item.
     */
    category?: Category;
    /**
     * Group this item belongs to.
     */
    group?: string;
    /**
     * Position of this item within its group.
     */
    groupPosition?: number;
    /**
     * Position of this item in cart.
     */
    position?: number;
}

/**
 * Category of this item.
 */
export enum Category {
    Food = "food",
    NonFood = "non-food",
    Others = "others",
}

/**
 * Product node either a real EAN/GTIN product or alternatively a generic one.
 */
export interface Product {
    /**
     * A generic or EAN/GTIN data based product for display.
     */
    data: Data;
    /**
     * Price of this cart item.
     */
    price: Price;
    /**
     * Quantifier of this cart item (e.g. 1 pcs. or 200 g).
     */
    quantifier: Quantifier;
}

/**
 * A generic or EAN/GTIN data based product for display.
 *
 * A generic product representation within the wunderbon domain.
 *
 * An EAN product representation within the wunderbon domain.
 */
export interface Data {
    /**
     * EAN (GTIN) 8||13 product code.
     */
    code: string;
    /**
     * A short product name.
     */
    name?: NameClass | string;
    /**
     * Product description.
     */
    description?: string;
    /**
     * Origin of the product in ISO representation.
     */
    origin?: Country;
    /**
     * Supplier or manufacturer of the product.
     */
    supplier?: string;
}

export interface NameClass {
    /**
     * Full product name.
     */
    long?: any;
    /**
     * Short product name.
     */
    short: any;
}

/**
 * Origin of the product in ISO representation.
 */
export enum Country {
    AE = "AE",
    AF = "AF",
    AI = "AI",
    Ad = "AD",
    Ag = "AG",
    Al = "AL",
    Am = "AM",
    Ao = "AO",
    Aq = "AQ",
    Ar = "AR",
    As = "AS",
    At = "AT",
    Au = "AU",
    Aw = "AW",
    Ax = "AX",
    Az = "AZ",
    BI = "BI",
    BT = "BT",
    BW = "BW",
    Ba = "BA",
    Bb = "BB",
    Bd = "BD",
    Be = "BE",
    Bf = "BF",
    Bg = "BG",
    Bh = "BH",
    Bj = "BJ",
    Bl = "BL",
    Bm = "BM",
    Bn = "BN",
    Bo = "BO",
    Bq = "BQ",
    Br = "BR",
    Bs = "BS",
    Bv = "BV",
    By = "BY",
    Bz = "BZ",
    CA = "CA",
    CD = "CD",
    CF = "CF",
    CG = "CG",
    CM = "CM",
    CN = "CN",
    CR = "CR",
    Cc = "CC",
    Ch = "CH",
    Ci = "CI",
    Ck = "CK",
    Cl = "CL",
    Co = "CO",
    Cu = "CU",
    Cv = "CV",
    Cw = "CW",
    Cx = "CX",
    Cy = "CY",
    Cz = "CZ",
    De = "DE",
    Dj = "DJ",
    Dk = "DK",
    Dm = "DM",
    Do = "DO",
    Dz = "DZ",
    Ec = "EC",
    Ee = "EE",
    Eg = "EG",
    Eh = "EH",
    Er = "ER",
    Es = "ES",
    Et = "ET",
    Fi = "FI",
    Fj = "FJ",
    Fk = "FK",
    Fm = "FM",
    Fo = "FO",
    Fr = "FR",
    GB = "GB",
    Ga = "GA",
    Gd = "GD",
    Ge = "GE",
    Gf = "GF",
    Gg = "GG",
    Gh = "GH",
    Gi = "GI",
    Gl = "GL",
    Gm = "GM",
    Gn = "GN",
    Gp = "GP",
    Gq = "GQ",
    Gr = "GR",
    Gs = "GS",
    Gt = "GT",
    Gu = "GU",
    Gw = "GW",
    Gy = "GY",
    HT = "HT",
    Hk = "HK",
    Hm = "HM",
    Hn = "HN",
    Hr = "HR",
    Hu = "HU",
    ID = "ID",
    IL = "IL",
    IM = "IM",
    IR = "IR",
    Ie = "IE",
    In = "IN",
    Io = "IO",
    Iq = "IQ",
    Is = "IS",
    It = "IT",
    Je = "JE",
    Jm = "JM",
    Jo = "JO",
    Jp = "JP",
    KM = "KM",
    Ke = "KE",
    Kg = "KG",
    Kh = "KH",
    Ki = "KI",
    Kn = "KN",
    Kp = "KP",
    Kr = "KR",
    Kw = "KW",
    Ky = "KY",
    Kz = "KZ",
    LB = "LB",
    LV = "LV",
    La = "LA",
    Lc = "LC",
    Li = "LI",
    Lk = "LK",
    Lr = "LR",
    Ls = "LS",
    Lt = "LT",
    Lu = "LU",
    Ly = "LY",
    MF = "MF",
    MS = "MS",
    MT = "MT",
    MX = "MX",
    Ma = "MA",
    Mc = "MC",
    Md = "MD",
    Me = "ME",
    Mg = "MG",
    Mh = "MH",
    Mk = "MK",
    Ml = "ML",
    Mm = "MM",
    Mn = "MN",
    Mo = "MO",
    Mp = "MP",
    Mq = "MQ",
    Mr = "MR",
    Mu = "MU",
    Mv = "MV",
    Mw = "MW",
    My = "MY",
    Mz = "MZ",
    NI = "NI",
    NP = "NP",
    Na = "NA",
    Nc = "NC",
    Ne = "NE",
    Nf = "NF",
    Ng = "NG",
    Nl = "NL",
    No = "NO",
    Nr = "NR",
    Nu = "NU",
    Nz = "NZ",
    Om = "OM",
    PE = "PE",
    PG = "PG",
    PR = "PR",
    PS = "PS",
    Pa = "PA",
    Pf = "PF",
    Ph = "PH",
    Pk = "PK",
    Pl = "PL",
    Pm = "PM",
    Pn = "PN",
    Pt = "PT",
    Pw = "PW",
    Py = "PY",
    QA = "QA",
    Re = "RE",
    Ro = "RO",
    Rs = "RS",
    Ru = "RU",
    Rw = "RW",
    SD = "SD",
    SE = "SE",
    Sa = "SA",
    Sb = "SB",
    Sc = "SC",
    Sg = "SG",
    Sh = "SH",
    Si = "SI",
    Sj = "SJ",
    Sk = "SK",
    Sl = "SL",
    Sm = "SM",
    Sn = "SN",
    So = "SO",
    Sr = "SR",
    Ss = "SS",
    St = "ST",
    Sv = "SV",
    Sx = "SX",
    Sy = "SY",
    Sz = "SZ",
    Tc = "TC",
    Td = "TD",
    Tf = "TF",
    Tg = "TG",
    Th = "TH",
    Tj = "TJ",
    Tk = "TK",
    Tl = "TL",
    Tm = "TM",
    Tn = "TN",
    To = "TO",
    Tr = "TR",
    Tt = "TT",
    Tv = "TV",
    Tw = "TW",
    Tz = "TZ",
    Ua = "UA",
    Ug = "UG",
    Um = "UM",
    Us = "US",
    Uy = "UY",
    Uz = "UZ",
    VG = "VG",
    Va = "VA",
    Vc = "VC",
    Ve = "VE",
    Vi = "VI",
    Vn = "VN",
    Vu = "VU",
    Wf = "WF",
    Ws = "WS",
    Ye = "YE",
    Yt = "YT",
    Za = "ZA",
    Zm = "ZM",
    Zw = "ZW",
}

/**
 * Price of this cart item.
 */
export interface Price {
    /**
     * Price INCLUDING tax (e.g. VAT = Value Added Tax or others).
     */
    gross: GrossClass;
    /**
     * Price EXCLUDING tax (e.g. VAT = Value Added Tax or others).
     */
    net: NetClass;
    /**
     * Taxes applied on this item (e.g. VAT = Value Added Tax or others).
     */
    tax: Tax;
}

/**
 * Price INCLUDING tax (e.g. VAT = Value Added Tax or others).
 */
export interface GrossClass {
    /**
     * Currency of amount.
     */
    currency: Currency;
    /**
     * The value of the amount.
     */
    value: number;
}

/**
 * Currency of amount.
 */
export enum Currency {
    AMD = "AMD",
    Aed = "AED",
    Afn = "AFN",
    All = "ALL",
    Ang = "ANG",
    Aoa = "AOA",
    Ars = "ARS",
    Aud = "AUD",
    Awg = "AWG",
    Azn = "AZN",
    BAM = "BAM",
    BSD = "BSD",
    Bbd = "BBD",
    Bdt = "BDT",
    Bgn = "BGN",
    Bhd = "BHD",
    Bif = "BIF",
    Bmd = "BMD",
    Bnd = "BND",
    Bob = "BOB",
    Bov = "BOV",
    Brl = "BRL",
    Btn = "BTN",
    Bwp = "BWP",
    Byr = "BYR",
    Bzd = "BZD",
    CAD = "CAD",
    CRC = "CRC",
    Cdf = "CDF",
    Che = "CHE",
    Chf = "CHF",
    Chw = "CHW",
    Clf = "CLF",
    Clp = "CLP",
    Cny = "CNY",
    Cop = "COP",
    Cou = "COU",
    Cuc = "CUC",
    Cup = "CUP",
    Cve = "CVE",
    Czk = "CZK",
    Djf = "DJF",
    Dkk = "DKK",
    Dop = "DOP",
    Dzd = "DZD",
    EGP = "EGP",
    Ern = "ERN",
    Etb = "ETB",
    Eur = "EUR",
    Fjd = "FJD",
    Fkp = "FKP",
    Gbp = "GBP",
    Gel = "GEL",
    Ghs = "GHS",
    Gip = "GIP",
    Gmd = "GMD",
    Gnf = "GNF",
    Gtq = "GTQ",
    Gyd = "GYD",
    Hkd = "HKD",
    Hnl = "HNL",
    Hrk = "HRK",
    Htg = "HTG",
    Huf = "HUF",
    Idr = "IDR",
    Ils = "ILS",
    Inr = "INR",
    Iqd = "IQD",
    Irr = "IRR",
    Isk = "ISK",
    Jmd = "JMD",
    Jod = "JOD",
    Jpy = "JPY",
    Kes = "KES",
    Kgs = "KGS",
    Khr = "KHR",
    Kmf = "KMF",
    Kpw = "KPW",
    Krw = "KRW",
    Kwd = "KWD",
    Kyd = "KYD",
    Kzt = "KZT",
    Lak = "LAK",
    Lbp = "LBP",
    Lkr = "LKR",
    Lrd = "LRD",
    Lsl = "LSL",
    Lyd = "LYD",
    Mad = "MAD",
    Mdl = "MDL",
    Mga = "MGA",
    Mkd = "MKD",
    Mmk = "MMK",
    Mnt = "MNT",
    Mop = "MOP",
    Mro = "MRO",
    Mur = "MUR",
    Mvr = "MVR",
    Mwk = "MWK",
    Mxn = "MXN",
    Mxv = "MXV",
    Myr = "MYR",
    Mzn = "MZN",
    NIO = "NIO",
    Nad = "NAD",
    Ngn = "NGN",
    Nok = "NOK",
    Npr = "NPR",
    Nzd = "NZD",
    OMR = "OMR",
    PHP = "PHP",
    Pab = "PAB",
    Pen = "PEN",
    Pgk = "PGK",
    Pkr = "PKR",
    Pln = "PLN",
    Pyg = "PYG",
    Qar = "QAR",
    Ron = "RON",
    Rsd = "RSD",
    Rub = "RUB",
    Rwf = "RWF",
    SSP = "SSP",
    SVC = "SVC",
    Sar = "SAR",
    Sbd = "SBD",
    Scr = "SCR",
    Sdg = "SDG",
    Sek = "SEK",
    Sgd = "SGD",
    Shp = "SHP",
    Sll = "SLL",
    Sos = "SOS",
    Srd = "SRD",
    Std = "STD",
    Syp = "SYP",
    Szl = "SZL",
    Thb = "THB",
    Tjs = "TJS",
    Tmt = "TMT",
    Tnd = "TND",
    Top = "TOP",
    Try = "TRY",
    Ttd = "TTD",
    Twd = "TWD",
    Tzs = "TZS",
    Uah = "UAH",
    Ugx = "UGX",
    Usd = "USD",
    Usn = "USN",
    Uyi = "UYI",
    Uyu = "UYU",
    Uzs = "UZS",
    Vef = "VEF",
    Vnd = "VND",
    Vuv = "VUV",
    Wst = "WST",
    XAG = "XAG",
    Xaf = "XAF",
    Xau = "XAU",
    Xba = "XBA",
    Xbb = "XBB",
    Xbc = "XBC",
    Xbd = "XBD",
    Xcd = "XCD",
    Xdr = "XDR",
    Xof = "XOF",
    Xpd = "XPD",
    Xpf = "XPF",
    Xpt = "XPT",
    Xsu = "XSU",
    Xts = "XTS",
    Xua = "XUA",
    Xxx = "XXX",
    Yer = "YER",
    Zar = "ZAR",
    Zmw = "ZMW",
    Zwl = "ZWL",
}

/**
 * Price EXCLUDING tax (e.g. VAT = Value Added Tax or others).
 */
export interface NetClass {
    /**
     * Currency of amount.
     */
    currency: Currency;
    /**
     * The value of the amount.
     */
    value: number;
}

/**
 * Taxes applied on this item (e.g. VAT = Value Added Tax or others).
 */
export interface Tax {
    /**
     * An amount representation within the wunderbon domain.
     */
    amount: AmountClass;
    /**
     * Percentage of tax.
     */
    percentage: number;
    /**
     * A symbol as abbreviation in overview.
     */
    symbol?: string;
    /**
     * Type of tax e.g. 'VAT'
     */
    type: Type;
}

/**
 * An amount representation within the wunderbon domain.
 */
export interface AmountClass {
    /**
     * Currency of amount.
     */
    currency: Currency;
    /**
     * The value of the amount.
     */
    value: number;
}

/**
 * Type of tax e.g. 'VAT'
 */
export enum Type {
    Gst = "GST",
    Vat = "VAT",
}

/**
 * Quantifier of this cart item (e.g. 1 pcs. or 200 g).
 */
export interface Quantifier {
    /**
     * Amount of this item in cart.
     */
    quantity: number;
    /**
     * Unit of the quantifier (e.g. kg, g, mm, m, pieces, ...).
     */
    unit: Unit;
}

/**
 * Unit of the quantifier (e.g. kg, g, mm, m, pieces, ...).
 */
export enum Unit {
    CM = "cm",
    Dm = "dm",
    Ft = "ft",
    G = "g",
    Gt = "Gt",
    In = "in",
    KM = "km",
    Kg = "kg",
    M = "m",
    MT = "Mt",
    Mg = "mg",
    Mi = "mi",
    Mm = "mm",
    Ng = "ng",
    PG = "pg",
    Pcs = "pcs",
    Sm = "sm",
    T = "t",
    Yd = "yd",
    Μg = "µg",
}

// Converts JSON strings to/from your types
// and asserts the results of JSON.parse at runtime
export class Convert {
    public static toCartItem(json: string): CartItem {
        return cast(JSON.parse(json), r("CartItem"));
    }

    public static cartItemToJson(value: CartItem): string {
        return JSON.stringify(uncast(value, r("CartItem")), null, 2);
    }
}

function invalidValue(typ: any, val: any): never {
    throw Error(`Invalid value ${JSON.stringify(val)} for type ${JSON.stringify(typ)}`);
}

function jsonToJSProps(typ: any): any {
    if (typ.jsonToJS === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.json] = { key: p.js, typ: p.typ });
        typ.jsonToJS = map;
    }
    return typ.jsonToJS;
}

function jsToJSONProps(typ: any): any {
    if (typ.jsToJSON === undefined) {
        var map: any = {};
        typ.props.forEach((p: any) => map[p.js] = { key: p.json, typ: p.typ });
        typ.jsToJSON = map;
    }
    return typ.jsToJSON;
}

function transform(val: any, typ: any, getProps: any): any {
    function transformPrimitive(typ: string, val: any): any {
        if (typeof typ === typeof val) return val;
        return invalidValue(typ, val);
    }

    function transformUnion(typs: any[], val: any): any {
        // val must validate against one typ in typs
        var l = typs.length;
        for (var i = 0; i < l; i++) {
            var typ = typs[i];
            try {
                return transform(val, typ, getProps);
            } catch (_) {}
        }
        return invalidValue(typs, val);
    }

    function transformEnum(cases: string[], val: any): any {
        if (cases.indexOf(val) !== -1) return val;
        return invalidValue(cases, val);
    }

    function transformArray(typ: any, val: any): any {
        // val must be an array with no invalid elements
        if (!Array.isArray(val)) return invalidValue("array", val);
        return val.map(el => transform(el, typ, getProps));
    }

    function transformDate(typ: any, val: any): any {
        if (val === null) {
            return null;
        }
        const d = new Date(val);
        if (isNaN(d.valueOf())) {
            return invalidValue("Date", val);
        }
        return d;
    }

    function transformObject(props: { [k: string]: any }, additional: any, val: any): any {
        if (val === null || typeof val !== "object" || Array.isArray(val)) {
            return invalidValue("object", val);
        }
        var result: any = {};
        Object.getOwnPropertyNames(props).forEach(key => {
            const prop = props[key];
            const v = Object.prototype.hasOwnProperty.call(val, key) ? val[key] : undefined;
            result[prop.key] = transform(v, prop.typ, getProps);
        });
        Object.getOwnPropertyNames(val).forEach(key => {
            if (!Object.prototype.hasOwnProperty.call(props, key)) {
                result[key] = transform(val[key], additional, getProps);
            }
        });
        return result;
    }

    if (typ === "any") return val;
    if (typ === null) {
        if (val === null) return val;
        return invalidValue(typ, val);
    }
    if (typ === false) return invalidValue(typ, val);
    while (typeof typ === "object" && typ.ref !== undefined) {
        typ = typeMap[typ.ref];
    }
    if (Array.isArray(typ)) return transformEnum(typ, val);
    if (typeof typ === "object") {
        return typ.hasOwnProperty("unionMembers") ? transformUnion(typ.unionMembers, val)
            : typ.hasOwnProperty("arrayItems")    ? transformArray(typ.arrayItems, val)
            : typ.hasOwnProperty("props")         ? transformObject(getProps(typ), typ.additional, val)
            : invalidValue(typ, val);
    }
    // Numbers can be parsed by Date but shouldn't be.
    if (typ === Date && typeof val !== "number") return transformDate(typ, val);
    return transformPrimitive(typ, val);
}

function cast<T>(val: any, typ: any): T {
    return transform(val, typ, jsonToJSProps);
}

function uncast<T>(val: T, typ: any): any {
    return transform(val, typ, jsToJSONProps);
}

function a(typ: any) {
    return { arrayItems: typ };
}

function u(...typs: any[]) {
    return { unionMembers: typs };
}

function o(props: any[], additional: any) {
    return { props, additional };
}

function m(additional: any) {
    return { props: [], additional };
}

function r(name: string) {
    return { ref: name };
}

const typeMap: any = {
    "CartItem": o([
        { json: "meta", js: "meta", typ: u(undefined, r("Meta")) },
        { json: "product", js: "product", typ: r("Product") },
    ], false),
    "Meta": o([
        { json: "category", js: "category", typ: u(undefined, r("Category")) },
        { json: "group", js: "group", typ: u(undefined, "") },
        { json: "groupPosition", js: "groupPosition", typ: u(undefined, 0) },
        { json: "position", js: "position", typ: u(undefined, 0) },
    ], false),
    "Product": o([
        { json: "data", js: "data", typ: r("Data") },
        { json: "price", js: "price", typ: r("Price") },
        { json: "quantifier", js: "quantifier", typ: r("Quantifier") },
    ], false),
    "Data": o([
        { json: "code", js: "code", typ: "" },
        { json: "name", js: "name", typ: u(undefined, u(r("NameClass"), "")) },
        { json: "description", js: "description", typ: u(undefined, "") },
        { json: "origin", js: "origin", typ: u(undefined, r("Country")) },
        { json: "supplier", js: "supplier", typ: u(undefined, "") },
    ], false),
    "NameClass": o([
        { json: "long", js: "long", typ: u(undefined, "any") },
        { json: "short", js: "short", typ: "any" },
    ], false),
    "Price": o([
        { json: "gross", js: "gross", typ: r("GrossClass") },
        { json: "net", js: "net", typ: r("NetClass") },
        { json: "tax", js: "tax", typ: r("Tax") },
    ], false),
    "GrossClass": o([
        { json: "currency", js: "currency", typ: r("Currency") },
        { json: "value", js: "value", typ: 3.14 },
    ], false),
    "NetClass": o([
        { json: "currency", js: "currency", typ: r("Currency") },
        { json: "value", js: "value", typ: 3.14 },
    ], false),
    "Tax": o([
        { json: "amount", js: "amount", typ: r("AmountClass") },
        { json: "percentage", js: "percentage", typ: 3.14 },
        { json: "symbol", js: "symbol", typ: u(undefined, "") },
        { json: "type", js: "type", typ: r("Type") },
    ], false),
    "AmountClass": o([
        { json: "currency", js: "currency", typ: r("Currency") },
        { json: "value", js: "value", typ: 3.14 },
    ], false),
    "Quantifier": o([
        { json: "quantity", js: "quantity", typ: 3.14 },
        { json: "unit", js: "unit", typ: r("Unit") },
    ], false),
    "Category": [
        "food",
        "non-food",
        "others",
    ],
    "Country": [
        "AE",
        "AF",
        "AI",
        "AD",
        "AG",
        "AL",
        "AM",
        "AO",
        "AQ",
        "AR",
        "AS",
        "AT",
        "AU",
        "AW",
        "AX",
        "AZ",
        "BI",
        "BT",
        "BW",
        "BA",
        "BB",
        "BD",
        "BE",
        "BF",
        "BG",
        "BH",
        "BJ",
        "BL",
        "BM",
        "BN",
        "BO",
        "BQ",
        "BR",
        "BS",
        "BV",
        "BY",
        "BZ",
        "CA",
        "CD",
        "CF",
        "CG",
        "CM",
        "CN",
        "CR",
        "CC",
        "CH",
        "CI",
        "CK",
        "CL",
        "CO",
        "CU",
        "CV",
        "CW",
        "CX",
        "CY",
        "CZ",
        "DE",
        "DJ",
        "DK",
        "DM",
        "DO",
        "DZ",
        "EC",
        "EE",
        "EG",
        "EH",
        "ER",
        "ES",
        "ET",
        "FI",
        "FJ",
        "FK",
        "FM",
        "FO",
        "FR",
        "GB",
        "GA",
        "GD",
        "GE",
        "GF",
        "GG",
        "GH",
        "GI",
        "GL",
        "GM",
        "GN",
        "GP",
        "GQ",
        "GR",
        "GS",
        "GT",
        "GU",
        "GW",
        "GY",
        "HT",
        "HK",
        "HM",
        "HN",
        "HR",
        "HU",
        "ID",
        "IL",
        "IM",
        "IR",
        "IE",
        "IN",
        "IO",
        "IQ",
        "IS",
        "IT",
        "JE",
        "JM",
        "JO",
        "JP",
        "KM",
        "KE",
        "KG",
        "KH",
        "KI",
        "KN",
        "KP",
        "KR",
        "KW",
        "KY",
        "KZ",
        "LB",
        "LV",
        "LA",
        "LC",
        "LI",
        "LK",
        "LR",
        "LS",
        "LT",
        "LU",
        "LY",
        "MF",
        "MS",
        "MT",
        "MX",
        "MA",
        "MC",
        "MD",
        "ME",
        "MG",
        "MH",
        "MK",
        "ML",
        "MM",
        "MN",
        "MO",
        "MP",
        "MQ",
        "MR",
        "MU",
        "MV",
        "MW",
        "MY",
        "MZ",
        "NI",
        "NP",
        "NA",
        "NC",
        "NE",
        "NF",
        "NG",
        "NL",
        "NO",
        "NR",
        "NU",
        "NZ",
        "OM",
        "PE",
        "PG",
        "PR",
        "PS",
        "PA",
        "PF",
        "PH",
        "PK",
        "PL",
        "PM",
        "PN",
        "PT",
        "PW",
        "PY",
        "QA",
        "RE",
        "RO",
        "RS",
        "RU",
        "RW",
        "SD",
        "SE",
        "SA",
        "SB",
        "SC",
        "SG",
        "SH",
        "SI",
        "SJ",
        "SK",
        "SL",
        "SM",
        "SN",
        "SO",
        "SR",
        "SS",
        "ST",
        "SV",
        "SX",
        "SY",
        "SZ",
        "TC",
        "TD",
        "TF",
        "TG",
        "TH",
        "TJ",
        "TK",
        "TL",
        "TM",
        "TN",
        "TO",
        "TR",
        "TT",
        "TV",
        "TW",
        "TZ",
        "UA",
        "UG",
        "UM",
        "US",
        "UY",
        "UZ",
        "VG",
        "VA",
        "VC",
        "VE",
        "VI",
        "VN",
        "VU",
        "WF",
        "WS",
        "YE",
        "YT",
        "ZA",
        "ZM",
        "ZW",
    ],
    "Currency": [
        "AMD",
        "AED",
        "AFN",
        "ALL",
        "ANG",
        "AOA",
        "ARS",
        "AUD",
        "AWG",
        "AZN",
        "BAM",
        "BSD",
        "BBD",
        "BDT",
        "BGN",
        "BHD",
        "BIF",
        "BMD",
        "BND",
        "BOB",
        "BOV",
        "BRL",
        "BTN",
        "BWP",
        "BYR",
        "BZD",
        "CAD",
        "CRC",
        "CDF",
        "CHE",
        "CHF",
        "CHW",
        "CLF",
        "CLP",
        "CNY",
        "COP",
        "COU",
        "CUC",
        "CUP",
        "CVE",
        "CZK",
        "DJF",
        "DKK",
        "DOP",
        "DZD",
        "EGP",
        "ERN",
        "ETB",
        "EUR",
        "FJD",
        "FKP",
        "GBP",
        "GEL",
        "GHS",
        "GIP",
        "GMD",
        "GNF",
        "GTQ",
        "GYD",
        "HKD",
        "HNL",
        "HRK",
        "HTG",
        "HUF",
        "IDR",
        "ILS",
        "INR",
        "IQD",
        "IRR",
        "ISK",
        "JMD",
        "JOD",
        "JPY",
        "KES",
        "KGS",
        "KHR",
        "KMF",
        "KPW",
        "KRW",
        "KWD",
        "KYD",
        "KZT",
        "LAK",
        "LBP",
        "LKR",
        "LRD",
        "LSL",
        "LYD",
        "MAD",
        "MDL",
        "MGA",
        "MKD",
        "MMK",
        "MNT",
        "MOP",
        "MRO",
        "MUR",
        "MVR",
        "MWK",
        "MXN",
        "MXV",
        "MYR",
        "MZN",
        "NIO",
        "NAD",
        "NGN",
        "NOK",
        "NPR",
        "NZD",
        "OMR",
        "PHP",
        "PAB",
        "PEN",
        "PGK",
        "PKR",
        "PLN",
        "PYG",
        "QAR",
        "RON",
        "RSD",
        "RUB",
        "RWF",
        "SSP",
        "SVC",
        "SAR",
        "SBD",
        "SCR",
        "SDG",
        "SEK",
        "SGD",
        "SHP",
        "SLL",
        "SOS",
        "SRD",
        "STD",
        "SYP",
        "SZL",
        "THB",
        "TJS",
        "TMT",
        "TND",
        "TOP",
        "TRY",
        "TTD",
        "TWD",
        "TZS",
        "UAH",
        "UGX",
        "USD",
        "USN",
        "UYI",
        "UYU",
        "UZS",
        "VEF",
        "VND",
        "VUV",
        "WST",
        "XAG",
        "XAF",
        "XAU",
        "XBA",
        "XBB",
        "XBC",
        "XBD",
        "XCD",
        "XDR",
        "XOF",
        "XPD",
        "XPF",
        "XPT",
        "XSU",
        "XTS",
        "XUA",
        "XXX",
        "YER",
        "ZAR",
        "ZMW",
        "ZWL",
    ],
    "Type": [
        "GST",
        "VAT",
    ],
    "Unit": [
        "cm",
        "dm",
        "ft",
        "g",
        "Gt",
        "in",
        "km",
        "kg",
        "m",
        "Mt",
        "mg",
        "mi",
        "mm",
        "ng",
        "pg",
        "pcs",
        "sm",
        "t",
        "yd",
        "µg",
    ],
};
