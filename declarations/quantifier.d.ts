// Generated by dts-bundle-generator v4.2.0

/**
 * wunderbon
 *
 * MIT License
 *
 * @copyright 2018 - 2020 wunderbon Operation GmbH & Co. KG - All rights reserved
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
/**
 * A quantifier representation within the wunderbon domain. Quantifier for quantity and unit
 * of item (e.g. 1 kg, 5 g, 3 pieces, ...).
 */
export interface Quantifier {
	/**
	 * Amount of this item in cart.
	 */
	quantity: number;
	/**
	 * Unit of the quantifier (e.g. kg, g, mm, m, pieces, ...).
	 */
	unit: Unit;
}
/**
 * Unit of the quantifier (e.g. kg, g, mm, m, pieces, ...).
 */
export declare enum Unit {
	CM = "cm",
	Dm = "dm",
	Ft = "ft",
	G = "g",
	Gt = "Gt",
	In = "in",
	KM = "km",
	Kg = "kg",
	M = "m",
	MT = "Mt",
	Mg = "mg",
	Mi = "mi",
	Mm = "mm",
	Ng = "ng",
	PG = "pg",
	Pcs = "pcs",
	Sm = "sm",
	T = "t",
	Yd = "yd",
	Μg = "\u00B5g"
}
export declare class Convert {
	static toQuantifier(json: string): Quantifier;
	static quantifierToJson(value: Quantifier): string;
}

export {};
